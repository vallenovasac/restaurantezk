package com.vallenova.restaurante.servicios

import grails.transaction.Transactional
import org.zkoss.zk.ui.*
import org.zkoss.zk.ui.event.*
import org.zkoss.zk.ui.util.*
import org.zkoss.zk.ui.ext.*
import org.zkoss.zk.au.*
import org.zkoss.zk.au.out.*
import org.zkoss.zul.*

@Transactional
class InsumoService {

    def getAllMedidas() {
		Insumo.unidaMedida.inList
    }
	
	def getAllInsumo() {
		Insumo.findAll()
	}
}
