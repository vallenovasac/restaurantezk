package com.vallenova.restaurante.modelo

class Insumo {

	String nombre
	String unidadMedida
	
	String toString() {
		nombre
	}
		
    static constraints = {
		nombre blank:false, size: 2..160
		unidadMedida inList:["Kilogramos","Litros"]
    }
}
