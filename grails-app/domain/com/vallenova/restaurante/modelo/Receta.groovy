package com.vallenova.restaurante.modelo

class Receta {

	String nombre
	
	String toString() {
		nombre
	}
	static hasMany = [ingredientes:Ingrediente]
	
    static constraints = {
		nombre blank:false, size:2..160
    }
}
