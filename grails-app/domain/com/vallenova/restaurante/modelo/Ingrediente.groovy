package com.vallenova.restaurante.modelo

class Ingrediente {

	static belongsTo = [insumo:Insumo, receta:Receta]
	BigDecimal cantidad
    
	String toString() {
		insumo
	}
	
	static constraints = {
    }
}
