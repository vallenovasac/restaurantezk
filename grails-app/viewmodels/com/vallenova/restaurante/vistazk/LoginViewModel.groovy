package com.vallenova.restaurante.vistazk

import org.zkoss.zk.grails.*

import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.select.annotation.Wire
import grails.plugin.springsecurity.SpringSecurityUtils

class LoginViewModel {

	/**
	 * Dependency injection for the authenticationTrustResolver.
	 */
	def authenticationTrustResolver

	/**
	 * Dependency injection for the springSecurityService.
	 */
	def springSecurityService
	

    @Init init() {
        // initialzation code here
//    	if (springSecurityService.isLoggedIn()) {
//			redirect url: SpringSecurityUtils.securityConfig.successHandler.defaultTargetUrl
//		} else {
//			//auth(params)
//		}
	}

	def auth(params) {
		def config = SpringSecurityUtils.securityConfig

		if (springSecurityService.isLoggedIn()) {
			redirect url: config.successHandler.defaultTargetUrl
			return
		}

		String view = 'auth'
		String postUrl = "${request.contextPath}${config.apf.filterProcessesUrl}"
		render view: view, model: [postUrl: postUrl,
		                           rememberMeParameter: config.rememberMe.parameter]
	}
}
