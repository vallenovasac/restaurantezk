package com.vallenova.restaurante.vistazk

import org.zkoss.zk.grails.*

import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.select.annotation.Wire
import com.vallenova.restaurante.modelo.Insumo

class ListaInsumoViewModel {

	//@Wire cbMedidas
	//String mensaje
	
    @Init init() {
        // initialzation code here
    }

	def getAllMedidas() {
		Insumo.constraints.unidadMedida.inList
	}
	
	def getAllInsumo() {
		Insumo.findAll()
	}
	
	//@NotifyChange(['mensaje'])
	//@Command cambioValor() {
		//mensaje = "Cambiado"
	//}
}
