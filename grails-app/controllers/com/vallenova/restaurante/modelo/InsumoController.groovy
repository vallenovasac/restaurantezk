package com.vallenova.restaurante.modelo



import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured;
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN'])
class InsumoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        //params.max = Math.min(max ?: 10, 100)
        //respond Insumo.list(params), model:[insumoInstanceCount: Insumo.count()]
    }

    def show(Insumo insumoInstance) {
        respond insumoInstance
    }

    def create() {
        respond new Insumo(params)
    }

    @Transactional
    def save(Insumo insumoInstance) {
        if (insumoInstance == null) {
            notFound()
            return
        }

        if (insumoInstance.hasErrors()) {
            respond insumoInstance.errors, view:'create'
            return
        }

        insumoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'insumoInstance.label', default: 'Insumo'), insumoInstance.id])
                redirect insumoInstance
            }
            '*' { respond insumoInstance, [status: CREATED] }
        }
    }

    def edit(Insumo insumoInstance) {
        respond insumoInstance
    }

    @Transactional
    def update(Insumo insumoInstance) {
        if (insumoInstance == null) {
            notFound()
            return
        }

        if (insumoInstance.hasErrors()) {
            respond insumoInstance.errors, view:'edit'
            return
        }

        insumoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Insumo.label', default: 'Insumo'), insumoInstance.id])
                redirect insumoInstance
            }
            '*'{ respond insumoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Insumo insumoInstance) {

        if (insumoInstance == null) {
            notFound()
            return
        }

        insumoInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Insumo.label', default: 'Insumo'), insumoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'insumoInstance.label', default: 'Insumo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
