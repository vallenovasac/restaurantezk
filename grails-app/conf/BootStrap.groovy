import com.vallenova.restaurante.seguridad.Role
import com.vallenova.restaurante.seguridad.User
import com.vallenova.restaurante.seguridad.UserRole;

class BootStrap {

    def init = { servletContext ->
		
		def adminRol = new Role(authority: 'ROLE_ADMIN')
		adminRol.save(flush: true)
		
		def adminUsuario = new User(username: 'admin', password: '1234')
		adminUsuario.nombreCompleto = 'Juan Perez'
		adminUsuario.save(flush: true)
		
		UserRole.create(adminUsuario, adminRol, true)
		
		assert User.count() == 1
		assert Role.count() == 1
		assert UserRole.count() == 1
    }
	
    def destroy = {
    }
}
