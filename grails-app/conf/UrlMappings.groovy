class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

		"/principal"(view:"/com/vallenova/restaurante/vistazk/principal.zul")
        "/"(view:"/com/vallenova/restaurante/vistazk/principal.zul")
        "500"(view:'/error')
		"/testlogin"(view:"/com/vallenova/restaurante/vistazk/inicio.zul")
	}
}
