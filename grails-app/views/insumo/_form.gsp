<%@ page import="com.vallenova.restaurante.modelo.Insumo" %>



<div class="fieldcontain ${hasErrors(bean: insumoInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="insumo.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" maxlength="160" required="" value="${insumoInstance?.nombre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: insumoInstance, field: 'unidadMedida', 'error')} ">
	<label for="unidadMedida">
		<g:message code="insumo.unidadMedida.label" default="Unidad Medida" />
		
	</label>
	<g:select name="unidadMedida" from="${insumoInstance.constraints.unidadMedida.inList}" value="${insumoInstance?.unidadMedida}" valueMessagePrefix="insumo.unidadMedida" noSelection="['': '']"/>
</div>

