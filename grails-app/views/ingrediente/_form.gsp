<%@ page import="com.vallenova.restaurante.modelo.Ingrediente" %>



<div class="fieldcontain ${hasErrors(bean: ingredienteInstance, field: 'cantidad', 'error')} required">
	<label for="cantidad">
		<g:message code="ingrediente.cantidad.label" default="Cantidad" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="cantidad" value="${fieldValue(bean: ingredienteInstance, field: 'cantidad')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: ingredienteInstance, field: 'insumo', 'error')} required">
	<label for="insumo">
		<g:message code="ingrediente.insumo.label" default="Insumo" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="insumo" name="insumo.id" from="${com.vallenova.restaurante.modelo.Insumo.list()}" optionKey="id" required="" value="${ingredienteInstance?.insumo?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ingredienteInstance, field: 'receta', 'error')} required">
	<label for="receta">
		<g:message code="ingrediente.receta.label" default="Receta" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="receta" name="receta.id" from="${com.vallenova.restaurante.modelo.Receta.list()}" optionKey="id" required="" value="${ingredienteInstance?.receta?.id}" class="many-to-one"/>
</div>

