<%@ page import="com.vallenova.restaurante.modelo.Receta" %>



<div class="fieldcontain ${hasErrors(bean: recetaInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="receta.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" maxlength="160" required="" value="${recetaInstance?.nombre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: recetaInstance, field: 'ingredientes', 'error')} ">
	<label for="ingredientes">
		<g:message code="receta.ingredientes.label" default="Ingredientes" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${recetaInstance?.ingredientes?}" var="i">
    <li><g:link controller="ingrediente" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="ingrediente" action="create" params="['receta.id': recetaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'ingrediente.label', default: 'Ingrediente')])}</g:link>
</li>
</ul>

</div>

