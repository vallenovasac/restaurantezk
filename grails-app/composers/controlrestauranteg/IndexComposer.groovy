package controlrestauranteg

import com.vallenova.restaurante.seguridad.User
import org.zkoss.zk.ui.event.Event
import org.zkoss.zk.ui.event.ForwardEvent
import org.zkoss.zul.Grid
import org.zkoss.zul.ListModelList
import org.zkoss.zul.RowRenderer
import com.vallenova.restaurante.modelo.Insumo


class IndexComposer extends zk.grails.Composer {

	private Grid listaInsumo
	def lista
	
    def afterCompose = {  window ->
		//def myRender = {Object[] args} as RowRenderer
		//listaInsumo.setRowRenderer( myRender) 
		lista = new ListModelList(Insumo.findAll())
		listaInsumo.setModel(lista)
	}
	
}
