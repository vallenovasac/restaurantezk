package com.vallenova.restaurante.modelo



import grails.test.mixin.*
import spock.lang.*

@TestFor(InsumoController)
@Mock(Insumo)
class InsumoControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.insumoInstanceList
            model.insumoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.insumoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def insumo = new Insumo()
            insumo.validate()
            controller.save(insumo)

        then:"The create view is rendered again with the correct model"
            model.insumoInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            insumo = new Insumo(params)

            controller.save(insumo)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/insumo/show/1'
            controller.flash.message != null
            Insumo.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def insumo = new Insumo(params)
            controller.show(insumo)

        then:"A model is populated containing the domain instance"
            model.insumoInstance == insumo
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def insumo = new Insumo(params)
            controller.edit(insumo)

        then:"A model is populated containing the domain instance"
            model.insumoInstance == insumo
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/insumo/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def insumo = new Insumo()
            insumo.validate()
            controller.update(insumo)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.insumoInstance == insumo

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            insumo = new Insumo(params).save(flush: true)
            controller.update(insumo)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/insumo/show/$insumo.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/insumo/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def insumo = new Insumo(params).save(flush: true)

        then:"It exists"
            Insumo.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(insumo)

        then:"The instance is deleted"
            Insumo.count() == 0
            response.redirectedUrl == '/insumo/index'
            flash.message != null
    }
}
